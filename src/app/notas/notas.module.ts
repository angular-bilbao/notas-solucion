import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AulaComponent } from './aula/aula.component';
import { AlumnosComponent } from './alumnos/alumnos.component';

@NgModule({
  declarations: [AulaComponent, AlumnosComponent],
  imports: [
    CommonModule
  ],
  exports: [
    AulaComponent
  ]
})
export class NotasModule { }
