import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EstadisticasComponent } from './estadisticas/estadisticas.component';
import { EstadisticaComponent } from './estadistica/estadistica.component';

@NgModule({
  declarations: [EstadisticasComponent, EstadisticaComponent],
  imports: [
    CommonModule
  ]
})
export class EstadisticasModule { }
